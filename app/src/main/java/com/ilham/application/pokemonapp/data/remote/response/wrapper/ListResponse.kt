package com.ilham.application.pokemonapp.data.remote.response.wrapper

import com.google.gson.annotations.SerializedName

data class ListResponse<T>(
    @SerializedName("count")
    val count: Int = 0,
    @SerializedName("results")
    val results: List<T>? = null
)