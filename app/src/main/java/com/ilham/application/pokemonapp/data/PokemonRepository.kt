package com.ilham.application.pokemonapp.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.ilham.application.pokemonapp.data.local.LocalDataSource
import com.ilham.application.pokemonapp.data.local.entity.CacheMapper
import com.ilham.application.pokemonapp.data.remote.NetworkMapper
import com.ilham.application.pokemonapp.data.remote.RemoteDataSource
import com.ilham.application.pokemonapp.data.remote.response.DetailPokemonResponse
import com.ilham.application.pokemonapp.data.remote.response.PokemonResponse
import com.ilham.application.pokemonapp.data.remote.response.wrapper.ListResponse
import com.ilham.application.pokemonapp.data.remote.vo.ApiResponse
import com.ilham.application.pokemonapp.model.Pokemon
import com.ilham.application.pokemonapp.model.PokemonStat
import com.ilham.application.pokemonapp.model.PokemonType
import com.ilham.application.pokemonapp.vo.Resource
import javax.inject.Inject

class PokemonRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val networkMapper: NetworkMapper,
    private val localDataSource: LocalDataSource,
    private val cacheMapper: CacheMapper
) :
    PokemonDataSource {

    override fun getListPokemon(): LiveData<Resource<List<Pokemon>>> {
        return object : NetworkBoundResource<List<Pokemon>, ListResponse<PokemonResponse>>() {
            override fun loadFromDB(): LiveData<List<Pokemon>> {
                return Transformations.map(localDataSource.getListPokemonFromCache()) { listPokemonCacheEntity ->
                    cacheMapper.mapListPokemonEntityToListPokemon(listPokemonCacheEntity)
                }
            }

            override fun shouldFetch(data: List<Pokemon>?): Boolean = data == null || data.isEmpty()

            override fun createCall(): LiveData<ApiResponse<ListResponse<PokemonResponse>>> =
                remoteDataSource.fetchListPokemonFromApi()

            override fun saveCallResult(data: ListResponse<PokemonResponse>) {
                val response = data.results?.let { listPokemonResponse ->
                    networkMapper.mapFromListPokemonResponse(listPokemonResponse)
                }

                response?.let { listPokemonResponse ->
                    localDataSource.insertPokemonToCache(
                        cacheMapper.mapListPokemonToEntity(
                            listPokemonResponse
                        )
                    )
                }
            }

        }.asLiveData()
    }

    override fun getPokemonById(pokemonId: Int): LiveData<Resource<Pokemon>> {
        return object : NetworkBoundResource<Pokemon, DetailPokemonResponse>() {
            override fun loadFromDB(): LiveData<Pokemon> {
                return Transformations.map(localDataSource.getPokemonFromCacheById(pokemonId)) { pokemonCacheEntity ->
                    cacheMapper.mapPokemonEntityToPokemon(pokemonCacheEntity)
                }
            }

            override fun shouldFetch(data: Pokemon?): Boolean = data == null

            override fun createCall(): LiveData<ApiResponse<DetailPokemonResponse>> =
                remoteDataSource.fetchDetailPokemonByIdFromApi(pokemonId)

            override fun saveCallResult(data: DetailPokemonResponse) {
                val detailPokemon = networkMapper.mapPokemonDetailFromResponse(data)

                val detailPokemonCache = cacheMapper.mapPokemonToPokemonEntity(detailPokemon)

                localDataSource.insertPokemonToCache(listOf(detailPokemonCache))
            }

        }.asLiveData()
    }

    override fun getPokemonStatsById(pokemonId: Int): LiveData<Resource<List<PokemonStat>>> {
        return object : NetworkBoundResource<List<PokemonStat>, DetailPokemonResponse>() {
            override fun loadFromDB(): LiveData<List<PokemonStat>> {
                return Transformations.map(
                    localDataSource.getListPokemonStatFromCacheById(
                        pokemonId
                    )
                ) { listPokemonStatCacheEntity ->
                    cacheMapper.mapListPokemonStatEntityToPokemonStat(listPokemonStatCacheEntity)
                }
            }

            override fun shouldFetch(data: List<PokemonStat>?): Boolean =
                data == null || data.isEmpty()

            override fun createCall(): LiveData<ApiResponse<DetailPokemonResponse>> =
                remoteDataSource.fetchDetailPokemonByIdFromApi(pokemonId)

            override fun saveCallResult(data: DetailPokemonResponse) {
                val responseResult = data.stats?.let { listPokemonStatResponse ->
                    networkMapper.mapFromListPokemonStatResponse(
                        listPokemonStatResponse
                    )
                }

                val pokemonStatCached = responseResult?.let { listPokemonStat ->
                    data.id?.let { pokemonId ->
                        cacheMapper.mapListPokemonStatToEntity(
                            pokemonId,
                            listPokemonStat
                        )
                    }
                }

                pokemonStatCached?.let { listPokemonStatCached ->
                    localDataSource.insertPokemonStatToCache(listPokemonStatCached)
                }

            }


        }.asLiveData()
    }

    override fun getPokemonTypesById(pokemonId: Int): LiveData<Resource<List<PokemonType>>> {
        return object : NetworkBoundResource<List<PokemonType>, DetailPokemonResponse>() {
            override fun loadFromDB(): LiveData<List<PokemonType>> {
                return Transformations.map(
                    localDataSource.getListPokemonTypeFromCacheById(
                        pokemonId
                    )
                ) { listPokemonTypeCacheEntity ->
                    cacheMapper.mapListPokemonTypeEntityToPokemonType(listPokemonTypeCacheEntity)
                }
            }

            override fun shouldFetch(data: List<PokemonType>?): Boolean =
                data == null || data.isEmpty()

            override fun createCall(): LiveData<ApiResponse<DetailPokemonResponse>> =
                remoteDataSource.fetchDetailPokemonByIdFromApi(pokemonId)

            override fun saveCallResult(data: DetailPokemonResponse) {
                val responseResult = data.types?.let { listPokemonTypeResponse ->
                    networkMapper.mapFromListPokemonTypeResponse(
                        listPokemonTypeResponse
                    )
                }

                val pokemonTypeCached = responseResult?.let { listPokemonType ->
                    data.id?.let { pokemonId ->
                        cacheMapper.mapListPokemonTypeToEntity(
                            pokemonId,
                            listPokemonType
                        )
                    }
                }

                pokemonTypeCached?.let { listPokemonTypeCached ->
                    localDataSource.insertPokemonTypeToCache(listPokemonTypeCached)
                }
            }

        }.asLiveData()
    }

}