package com.ilham.application.pokemonapp.di

import com.ilham.application.pokemonapp.data.PokemonRepository
import com.ilham.application.pokemonapp.data.local.LocalDataSource
import com.ilham.application.pokemonapp.data.local.dao.PokemonDao
import com.ilham.application.pokemonapp.data.local.entity.CacheMapper
import com.ilham.application.pokemonapp.data.remote.NetworkMapper
import com.ilham.application.pokemonapp.data.remote.RemoteDataSource
import com.ilham.application.pokemonapp.data.remote.api.PokemonApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideLocalDataSource(pokemonDao: PokemonDao): LocalDataSource =
        LocalDataSource(pokemonDao)

    @Singleton
    @Provides
    fun provideCacheMapper(): CacheMapper = CacheMapper

    @Singleton
    @Provides
    fun provideRemoteDataSource(pokemonApiService: PokemonApiService): RemoteDataSource =
        RemoteDataSource(pokemonApiService)

    @Singleton
    @Provides
    fun provideNetworkMapper(): NetworkMapper = NetworkMapper

    @Singleton
    @Provides
    fun providePokemonRepository(
        remoteDataSource: RemoteDataSource,
        networkMapper: NetworkMapper,
        localDataSource: LocalDataSource,
        cacheMapper: CacheMapper
    ): PokemonRepository =
        PokemonRepository(remoteDataSource, networkMapper, localDataSource, cacheMapper)

}