package com.ilham.application.pokemonapp.data.remote

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ilham.application.pokemonapp.data.remote.api.PokemonApiService
import com.ilham.application.pokemonapp.data.remote.response.DetailPokemonResponse
import com.ilham.application.pokemonapp.data.remote.response.PokemonResponse
import com.ilham.application.pokemonapp.data.remote.response.wrapper.ListResponse
import com.ilham.application.pokemonapp.data.remote.vo.ApiResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val pokemonApiService: PokemonApiService) {

    fun fetchListPokemonFromApi(): LiveData<ApiResponse<ListResponse<PokemonResponse>>> {
        val results = MutableLiveData<ApiResponse<ListResponse<PokemonResponse>>>()

        CoroutineScope(IO).launch {
            try {
                val response = pokemonApiService.getPokemonList()

                when (response.isSuccessful) {
                    true -> {
                        response.body()?.let { listPokemonResponse ->
                            results.postValue(ApiResponse.Success(listPokemonResponse))
                        }
                    }
                    false -> {
                        results.postValue(
                            ApiResponse.Error("${response.code()} : ${response.errorBody()}")
                        )
                    }
                }
            } catch (e: Exception) {
                e.message?.let { errorMsg ->
                    results.postValue(ApiResponse.Error(errorMsg))
                } ?: run {
                    results.postValue(ApiResponse.Error("You have no internet connection"))
                }

            }
        }

        return results
    }

    fun fetchDetailPokemonByIdFromApi(pokemonId: Int): LiveData<ApiResponse<DetailPokemonResponse>> {
        val result = MutableLiveData<ApiResponse<DetailPokemonResponse>>()

        CoroutineScope(IO).launch {
            try {
                val response = pokemonApiService.getDetailPokemonById(pokemonId)

                when (response.isSuccessful) {
                    true -> {
                        response.body()?.let { detailPokemonResponse ->
                            result.postValue(ApiResponse.Success(detailPokemonResponse))
                        }
                    }

                    false -> {
                        result.postValue(
                            ApiResponse.Error("${response.code()} : ${response.errorBody()}")
                        )
                    }
                }
            } catch (e: Exception) {
                e.message?.let { errorMsg ->
                    result.postValue(ApiResponse.Error(errorMsg))
                }.run {
                    result.postValue(ApiResponse.Error("You have no internet connection"))
                }
            }
        }

        return result
    }
}