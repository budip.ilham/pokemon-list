package com.ilham.application.pokemonapp.data.remote

import com.ilham.application.pokemonapp.data.remote.response.DetailPokemonResponse
import com.ilham.application.pokemonapp.data.remote.response.PokemonResponse
import com.ilham.application.pokemonapp.data.remote.response.PokemonStatResponse
import com.ilham.application.pokemonapp.data.remote.response.PokemonTypeResponse
import com.ilham.application.pokemonapp.model.Pokemon
import com.ilham.application.pokemonapp.model.PokemonStat
import com.ilham.application.pokemonapp.model.PokemonType
import com.ilham.application.pokemonapp.utils.Constants
import com.ilham.application.pokemonapp.utils.Helper

object NetworkMapper {

    private fun mapPokemonFromResponse(response: PokemonResponse): Pokemon =
        Pokemon(
            id = Helper.getIdFromSegmentedUrl(response.url!!, 1).toInt(),
            name = response.name,
            image = Constants.POKEMON_IMAGE_URL + Helper.getIdFromSegmentedUrl(response.url, 1)
                .toInt() + Constants.POKEMON_IMAGE_EXTENSION
        )

    fun mapPokemonDetailFromResponse(response: DetailPokemonResponse): Pokemon =
        Pokemon(
            id = response.id,
            name = response.name,
            image = Constants.POKEMON_IMAGE_URL + response.id + Constants.POKEMON_IMAGE_EXTENSION
        )

    fun mapFromListPokemonResponse(responses: List<PokemonResponse>): List<Pokemon> =
        responses.map {
            mapPokemonFromResponse(it)
        }

    private fun mapPokemonStatFromResponse(response: PokemonStatResponse): PokemonStat =
        PokemonStat(
            name = response.stat?.name,
            baseStat = response.baseStat
        )

    fun mapFromListPokemonStatResponse(responses: List<PokemonStatResponse>): List<PokemonStat> =
        responses.map {
            mapPokemonStatFromResponse(it)
        }

    private fun mapPokemonTypeFromResponse(response: PokemonTypeResponse): PokemonType =
        PokemonType(
            slot = response.slot,
            name = response.type?.name
        )

    fun mapFromListPokemonTypeResponse(responses: List<PokemonTypeResponse>): List<PokemonType> =
        responses.map {
            mapPokemonTypeFromResponse(it)
        }

}