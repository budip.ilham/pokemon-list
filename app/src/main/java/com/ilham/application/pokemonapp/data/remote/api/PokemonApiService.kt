package com.ilham.application.pokemonapp.data.remote.api

import com.ilham.application.pokemonapp.data.remote.response.DetailPokemonResponse
import com.ilham.application.pokemonapp.data.remote.response.PokemonResponse
import com.ilham.application.pokemonapp.data.remote.response.wrapper.ListResponse
import com.ilham.application.pokemonapp.utils.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonApiService {

    @GET("pokemon")
    suspend fun getPokemonList(
        @Query("offset") offset: Int? = Constants.DEFAULT_OFFSET,
        @Query("limit") limit: Int? = Constants.DEFAULT_LIMIT
    ): Response<ListResponse<PokemonResponse>>

    @GET("pokemon/{id}")
    suspend fun getDetailPokemonById(
        @Path("id") id: Int
    ): Response<DetailPokemonResponse>

}