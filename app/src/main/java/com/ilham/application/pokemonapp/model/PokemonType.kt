package com.ilham.application.pokemonapp.model

data class PokemonType(
    val slot: Int? = null,
    val name: String? = null
)