package com.ilham.application.pokemonapp.data.remote.response

import com.google.gson.annotations.SerializedName

data class PokemonTypeResponse(
    @SerializedName("slot")
    val slot: Int? = null,
    @SerializedName("type")
    val type: ChildDetailResponse? = null
)