package com.ilham.application.pokemonapp.ui.main

import com.ilham.application.pokemonapp.model.Pokemon

interface PokemonAdapterCallback {
    fun onItemClicked(data: Pokemon)
}