package com.ilham.application.pokemonapp.ui.detail

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.ilham.application.pokemonapp.data.PokemonRepository
import com.ilham.application.pokemonapp.model.Pokemon
import com.ilham.application.pokemonapp.model.PokemonStat
import com.ilham.application.pokemonapp.model.PokemonType
import com.ilham.application.pokemonapp.vo.Resource

class DetailViewModel @ViewModelInject constructor(
    private val pokemonRepository: PokemonRepository
) :
    ViewModel() {

    private var pokemonId = 0

    fun getDetailPokemonWithId(id: Int) {
        pokemonId = id
    }

    fun observePokemonDataState(): LiveData<Resource<Pokemon>> =
        pokemonRepository.getPokemonById(pokemonId)

    fun observePokemonStatsDataState(): LiveData<Resource<List<PokemonStat>>> =
        pokemonRepository.getPokemonStatsById(pokemonId)

    fun observePokemonTypesDataState(): LiveData<Resource<List<PokemonType>>> =
        pokemonRepository.getPokemonTypesById(pokemonId)

}

