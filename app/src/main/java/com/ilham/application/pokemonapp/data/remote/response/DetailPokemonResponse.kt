package com.ilham.application.pokemonapp.data.remote.response

import com.google.gson.annotations.SerializedName

data class DetailPokemonResponse(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("stats")
    val stats: List<PokemonStatResponse>? = null,
    @SerializedName("types")
    val types: List<PokemonTypeResponse>? = null
)