package com.ilham.application.pokemonapp.data.local

import androidx.lifecycle.LiveData
import com.ilham.application.pokemonapp.data.local.dao.PokemonDao
import com.ilham.application.pokemonapp.data.local.entity.pokemon.PokemonCacheEntity
import com.ilham.application.pokemonapp.data.local.entity.pokemon.PokemonStatCacheEntity
import com.ilham.application.pokemonapp.data.local.entity.pokemon.PokemonTypeCacheEntity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

class LocalDataSource @Inject constructor(private val pokemonDao: PokemonDao) {

    fun insertPokemonToCache(listCachedPokemon: List<PokemonCacheEntity>) =
        CoroutineScope(IO).launch {
            pokemonDao.insertPokemonCache(listCachedPokemon)
        }

    fun insertPokemonStatToCache(listCachedPokemonStat: List<PokemonStatCacheEntity>) =
        CoroutineScope(IO).launch {
            pokemonDao.insertPokemonStatCache(listCachedPokemonStat)
        }

    fun insertPokemonTypeToCache(listCachedPokemonType: List<PokemonTypeCacheEntity>) =
        CoroutineScope(IO).launch {
            pokemonDao.insertPokemonTypeCache(listCachedPokemonType)
        }

    fun getPokemonFromCacheById(pokemonId: Int): LiveData<PokemonCacheEntity> =
        pokemonDao.getCachedPokemon(pokemonId)

    fun getListPokemonStatFromCacheById(pokemonId: Int): LiveData<List<PokemonStatCacheEntity>> =
        pokemonDao.getCachedPokemonStatById(pokemonId)

    fun getListPokemonTypeFromCacheById(pokemonId: Int): LiveData<List<PokemonTypeCacheEntity>> =
        pokemonDao.getCachedPokemonTypeById(pokemonId)

    fun getListPokemonFromCache(): LiveData<List<PokemonCacheEntity>> =
        pokemonDao.getCachedPokemonList()


}