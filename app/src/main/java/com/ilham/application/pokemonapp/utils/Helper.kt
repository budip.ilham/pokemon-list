package com.ilham.application.pokemonapp.utils

object Helper {

    fun getIdFromSegmentedUrl(url: String, indexFromBehind: Int = 0): String {
        val splitString = url.split("/")
        return splitString[splitString.lastIndex - indexFromBehind]
    }

}