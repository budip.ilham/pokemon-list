package com.ilham.application.pokemonapp.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ilham.application.pokemonapp.R
import com.ilham.application.pokemonapp.model.Pokemon
import com.ilham.application.pokemonapp.utils.loadImageFromUrl
import kotlinx.android.synthetic.main.item_row_pokemon.view.*

class PokemonAdapter(private val callback: PokemonAdapterCallback) :
    RecyclerView.Adapter<PokemonAdapter.ListViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<Pokemon>() {
        override fun areItemsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
            return oldItem == newItem
        }

    }

    val differ = AsyncListDiffer(this, diffCallback)

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(data: Pokemon) {
            with(itemView) {
                tvPokemonName.text = data.name
                data.image?.let {
                    imgPokemon.loadImageFromUrl(it)
                }

                setOnClickListener {
                    callback.onItemClicked(data)
                }

            }

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder =
        ListViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_row_pokemon, parent, false)
        )

    override fun getItemCount(): Int = differ.currentList.size

    override fun onBindViewHolder(holder: PokemonAdapter.ListViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

}