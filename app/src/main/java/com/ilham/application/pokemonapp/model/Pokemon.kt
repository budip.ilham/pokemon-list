package com.ilham.application.pokemonapp.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Pokemon(
    val id: Int? = null,
    val name: String? = null,
    val image: String? = null
) : Parcelable