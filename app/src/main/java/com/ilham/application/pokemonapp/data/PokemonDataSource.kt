package com.ilham.application.pokemonapp.data

import androidx.lifecycle.LiveData
import com.ilham.application.pokemonapp.model.Pokemon
import com.ilham.application.pokemonapp.model.PokemonStat
import com.ilham.application.pokemonapp.model.PokemonType
import com.ilham.application.pokemonapp.vo.Resource

interface PokemonDataSource {

    fun getListPokemon(): LiveData<Resource<List<Pokemon>>>

    fun getPokemonById(pokemonId: Int): LiveData<Resource<Pokemon>>

    fun getPokemonStatsById(pokemonId: Int): LiveData<Resource<List<PokemonStat>>>

    fun getPokemonTypesById(pokemonId: Int): LiveData<Resource<List<PokemonType>>>

}