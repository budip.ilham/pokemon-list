package com.ilham.application.pokemonapp.data.remote.response

import com.google.gson.annotations.SerializedName

data class PokemonStatResponse(
    @SerializedName("base_stat")
    val baseStat: Int? = null,
    @SerializedName("stat")
    val stat: ChildDetailResponse? = null
)