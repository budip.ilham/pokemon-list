package com.ilham.application.pokemonapp.data.local.entity

import com.ilham.application.pokemonapp.data.local.entity.pokemon.PokemonCacheEntity
import com.ilham.application.pokemonapp.data.local.entity.pokemon.PokemonStatCacheEntity
import com.ilham.application.pokemonapp.data.local.entity.pokemon.PokemonTypeCacheEntity
import com.ilham.application.pokemonapp.model.Pokemon
import com.ilham.application.pokemonapp.model.PokemonStat
import com.ilham.application.pokemonapp.model.PokemonType

object CacheMapper {

    /**
     * Pokemon
     */

    fun mapPokemonToPokemonEntity(pokemon: Pokemon): PokemonCacheEntity =
        PokemonCacheEntity(
            id = pokemon.id,
            name = pokemon.name,
            image = pokemon.image
        )

    fun mapPokemonEntityToPokemon(pokemonCacheEntity: PokemonCacheEntity): Pokemon =
        Pokemon(
            id = pokemonCacheEntity.id,
            name = pokemonCacheEntity.name,
            image = pokemonCacheEntity.image
        )

    fun mapListPokemonToEntity(pokemon: List<Pokemon>): List<PokemonCacheEntity> =
        pokemon.map {
            mapPokemonToPokemonEntity(it)
        }

    fun mapListPokemonEntityToListPokemon(cachedPokemonList: List<PokemonCacheEntity>): List<Pokemon> =
        cachedPokemonList.map {
            mapPokemonEntityToPokemon(it)
        }

    /**
     * Pokemon Stat
     */

    private fun mapPokemonStatToEntity(
        pokemonId: Int,
        pokemonStat: PokemonStat
    ): PokemonStatCacheEntity =
        PokemonStatCacheEntity(
            id = null,
            pokemonId = pokemonId,
            name = pokemonStat.name,
            baseStat = pokemonStat.baseStat
        )

    private fun mapPokemonStatEntityToPokemonStat(cachedPokemonStat: PokemonStatCacheEntity): PokemonStat =
        PokemonStat(
            name = cachedPokemonStat.name,
            baseStat = cachedPokemonStat.baseStat
        )

    fun mapListPokemonStatToEntity(
        pokemonId: Int,
        pokemonStatList: List<PokemonStat>
    ): List<PokemonStatCacheEntity> = pokemonStatList.map {
        mapPokemonStatToEntity(pokemonId, it)
    }

    fun mapListPokemonStatEntityToPokemonStat(cachedPokemonStatList: List<PokemonStatCacheEntity>): List<PokemonStat> =
        cachedPokemonStatList.map {
            mapPokemonStatEntityToPokemonStat(it)
        }

    /**
     * Pokemon Type
     */

    private fun mapPokemonTypeToEntity(
        pokemonId: Int,
        pokemonType: PokemonType
    ): PokemonTypeCacheEntity =
        PokemonTypeCacheEntity(
            id = null,
            pokemonId = pokemonId,
            name = pokemonType.name,
            slot = pokemonType.slot
        )

    private fun mapPokemonTypeEntityToPokemonType(cachedPokemonTypeEntity: PokemonTypeCacheEntity): PokemonType =
        PokemonType(
            slot = cachedPokemonTypeEntity.slot,
            name = cachedPokemonTypeEntity.name
        )

    fun mapListPokemonTypeToEntity(
        pokemonId: Int,
        pokemonTypeList: List<PokemonType>
    ): List<PokemonTypeCacheEntity> = pokemonTypeList.map {
        mapPokemonTypeToEntity(pokemonId, it)
    }

    fun mapListPokemonTypeEntityToPokemonType(cachedPokemonTypeEntity: List<PokemonTypeCacheEntity>): List<PokemonType> =
        cachedPokemonTypeEntity.map {
            mapPokemonTypeEntityToPokemonType(it)
        }

}