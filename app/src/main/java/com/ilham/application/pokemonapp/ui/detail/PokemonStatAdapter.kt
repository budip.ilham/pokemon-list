package com.ilham.application.pokemonapp.ui.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ilham.application.pokemonapp.R
import com.ilham.application.pokemonapp.model.PokemonStat
import com.ilham.application.pokemonapp.utils.Constants.DEFAULT_BASE_STAT
import com.ilham.application.pokemonapp.utils.setStat
import kotlinx.android.synthetic.main.item_row_stat.view.*


class PokemonStatAdapter :
    RecyclerView.Adapter<PokemonStatAdapter.ListViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<PokemonStat>() {
        override fun areItemsTheSame(oldItem: PokemonStat, newItem: PokemonStat): Boolean {
            return oldItem.name == newItem.name
        }

        override fun areContentsTheSame(oldItem: PokemonStat, newItem: PokemonStat): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, diffCallback)

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(data: PokemonStat) {
            with(itemView) {
                data.name?.let { statName ->
                    tvLabelStat.text = when (statName) {
                        resources.getString(R.string.pokemonStatNameHP) -> resources.getString(R.string.pokemonStatusHP)
                        resources.getString(R.string.pokemonStatNameAttack) -> resources.getString(R.string.pokemonStatusAttack)
                        resources.getString(R.string.pokemonStatNameDefense) -> resources.getString(
                            R.string.pokemonStatusDefense
                        )
                        resources.getString(R.string.pokemonStatNameSpecialAttack) -> resources.getString(
                            R.string.pokemonStatusSpecialAttack
                        )
                        resources.getString(R.string.pokemonStatNameSpecialDefense) -> resources.getString(
                            R.string.pokemonStatusSpecialDefense
                        )
                        resources.getString(R.string.pokemonStatNameSpeed) -> resources.getString(R.string.pokemonStatusSpeed)
                        else -> resources.getString(R.string.undefined)
                    }

                    val statValue = data.baseStat ?: DEFAULT_BASE_STAT
                    statBar.setStat(statValue)
                    tvLabelStatValue.text =
                        resources.getString(R.string.StatsValue, statValue)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder =
        ListViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_row_stat, parent, false)
        )

    override fun getItemCount(): Int = differ.currentList.size

    override fun onBindViewHolder(holder: PokemonStatAdapter.ListViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

}