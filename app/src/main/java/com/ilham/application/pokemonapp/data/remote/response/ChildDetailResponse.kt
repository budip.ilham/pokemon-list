package com.ilham.application.pokemonapp.data.remote.response

import com.google.gson.annotations.SerializedName

data class ChildDetailResponse(
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("url")
    val url: String? = null
)