package com.ilham.application.pokemonapp.utils

object Constants {
    const val POKEMON_IMAGE_URL =
        "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
    const val POKEMON_IMAGE_EXTENSION = ".png"
    const val DEFAULT_OFFSET = 0
    const val DEFAULT_LIMIT = 20
    const val DEFAULT_BASE_STAT = 0
}