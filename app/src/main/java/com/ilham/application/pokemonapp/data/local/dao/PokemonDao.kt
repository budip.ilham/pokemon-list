package com.ilham.application.pokemonapp.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.ilham.application.pokemonapp.data.local.entity.pokemon.PokemonCacheEntity
import com.ilham.application.pokemonapp.data.local.entity.pokemon.PokemonStatCacheEntity
import com.ilham.application.pokemonapp.data.local.entity.pokemon.PokemonTypeCacheEntity

@Dao
interface PokemonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = PokemonCacheEntity::class)
    suspend fun insertPokemonCache(listPokemonCacheEntity: List<PokemonCacheEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = PokemonStatCacheEntity::class)
    suspend fun insertPokemonStatCache(listPokemonStatCacheEntity: List<PokemonStatCacheEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE, entity = PokemonTypeCacheEntity::class)
    suspend fun insertPokemonTypeCache(listPokemonTypeCacheEntity: List<PokemonTypeCacheEntity>)

    @Query("SELECT * FROM pokemon")
    fun getCachedPokemonList(): LiveData<List<PokemonCacheEntity>>

    @Query("SELECT * FROM pokemon WHERE id = :pokemonId")
    fun getCachedPokemon(pokemonId: Int): LiveData<PokemonCacheEntity>

    @Query("SELECT * FROM pokemon_stat WHERE pokemon_id = :pokemonId")
    fun getCachedPokemonStatById(pokemonId: Int): LiveData<List<PokemonStatCacheEntity>>

    @Query("SELECT * FROM pokemon_type WHERE pokemon_id = :pokemonId")
    fun getCachedPokemonTypeById(pokemonId: Int): LiveData<List<PokemonTypeCacheEntity>>

}