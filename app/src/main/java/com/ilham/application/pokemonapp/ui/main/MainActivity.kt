package com.ilham.application.pokemonapp.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.ilham.application.pokemonapp.R
import com.ilham.application.pokemonapp.model.Pokemon
import com.ilham.application.pokemonapp.ui.detail.DetailActivity
import com.ilham.application.pokemonapp.utils.gone
import com.ilham.application.pokemonapp.utils.showToast
import com.ilham.application.pokemonapp.utils.visible
import com.ilham.application.pokemonapp.vo.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity(), PokemonAdapterCallback {

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupRecyclerView()
        observePokemonList()

    }

    private fun setupRecyclerView() {
        rvPokemon.apply {
            layoutManager = GridLayoutManager(this@MainActivity, 2)
            adapter = PokemonAdapter(this@MainActivity)
        }
    }

    private fun observePokemonList() {
        viewModel.observeListDataState().observe(this, Observer {
            it?.let { pokemonListDataState ->

                rvPokemon.adapter?.let { adapter ->
                    when (adapter) {
                        is PokemonAdapter -> {
                            when (pokemonListDataState) {
                                is Resource.Success -> {
                                    adapter.differ.submitList(pokemonListDataState.data)
                                    displayProgressBar(false)
                                }
                                is Resource.Error -> {
                                    showToast(pokemonListDataState.message)
                                    mainProgressBar.gone()
                                }
                                is Resource.Loading -> {
                                    displayProgressBar(true)
                                }
                            }
                        }
                    }
                }

            }
        })
    }

    private fun displayProgressBar(isDisplayed: Boolean) {
        if (isDisplayed) {
            rvPokemon.gone()
            mainProgressBar.visible()
        } else {
            rvPokemon.visible()
            mainProgressBar.gone()
        }
    }


    override fun onItemClicked(data: Pokemon) {
        startActivity(
            Intent(this, DetailActivity::class.java)
                .putExtra(DetailActivity.EXTRA_DATA_ID, data.id)
        )
    }
}