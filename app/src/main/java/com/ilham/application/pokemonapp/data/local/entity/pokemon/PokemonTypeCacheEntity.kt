package com.ilham.application.pokemonapp.data.local.entity.pokemon

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "pokemon_type")
data class PokemonTypeCacheEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int? = null,

    @ColumnInfo(name = "pokemon_id")
    var pokemonId: Int? = null,

    @ColumnInfo(name = "slot")
    var slot: Int? = null,

    @ColumnInfo(name = "name")
    var name: String? = null

)