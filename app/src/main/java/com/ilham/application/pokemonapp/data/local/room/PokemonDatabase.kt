package com.ilham.application.pokemonapp.data.local.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.ilham.application.pokemonapp.data.local.entity.pokemon.PokemonStatCacheEntity
import com.ilham.application.pokemonapp.data.local.entity.pokemon.PokemonTypeCacheEntity
import com.ilham.application.pokemonapp.data.local.entity.pokemon.PokemonCacheEntity
import com.ilham.application.pokemonapp.data.local.dao.PokemonDao

@Database(
    entities = [
        PokemonCacheEntity::class,
        PokemonStatCacheEntity::class,
        PokemonTypeCacheEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class PokemonDatabase : RoomDatabase() {

    abstract fun pokemonDao(): PokemonDao

    companion object {
        const val DATABASE_NAME = "pokemon_db"
    }

}