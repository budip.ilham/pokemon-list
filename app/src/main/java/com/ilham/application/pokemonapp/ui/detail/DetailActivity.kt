package com.ilham.application.pokemonapp.ui.detail

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.ilham.application.pokemonapp.R
import com.ilham.application.pokemonapp.model.Pokemon
import com.ilham.application.pokemonapp.model.PokemonStat
import com.ilham.application.pokemonapp.model.PokemonType
import com.ilham.application.pokemonapp.utils.gone
import com.ilham.application.pokemonapp.utils.loadImageFromUrl
import com.ilham.application.pokemonapp.utils.showToast
import com.ilham.application.pokemonapp.utils.visible
import com.ilham.application.pokemonapp.vo.Resource
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class DetailActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_DATA_ID = "extra_data_id"
    }

    private val viewModel: DetailViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setupToolbar()

        val pokemonId = intent.getIntExtra(EXTRA_DATA_ID, 0)

        setupRecyclerView()
        getDetailPokemonWithId(pokemonId)
        observePokemon()
        observePokemonStats()
        observePokemonTypes()
    }

    private fun setupToolbar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(R.string.detailPokemon)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun setupRecyclerView() {
        rvPokemonTypes.apply {
            layoutManager =
                LinearLayoutManager(this@DetailActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = PokemonTypeAdapter()
        }

        rvPokemonStats.apply {
            layoutManager = LinearLayoutManager(this@DetailActivity)
            adapter = PokemonStatAdapter()
        }
    }

    private fun getDetailPokemonWithId(pokemonId: Int) {
        viewModel.getDetailPokemonWithId(pokemonId)
    }

    private fun observePokemon() {
        viewModel.observePokemonDataState().observe(this, Observer {
            it?.let { pokemonDataState ->
                when (pokemonDataState) {
                    is Resource.Error -> {
                        showToast(pokemonDataState.message)
                    }

                    is Resource.Success -> {
                        setViewWithPokemon(pokemonDataState.data)
                    }
                }
            }
        })
    }

    private fun observePokemonStats() {
        viewModel.observePokemonStatsDataState().observe(this, Observer {
            it?.let { pokemonStatsDataState ->
                when (pokemonStatsDataState) {
                    is Resource.Loading -> {
                        displayPokemonStatProgressBar(true)
                    }

                    is Resource.Error -> {
                        showToast(pokemonStatsDataState.message)
                        displayPokemonStatProgressBar(false)
                    }

                    is Resource.Success -> {
                        setViewWithPokemonStats(pokemonStatsDataState.data)
                        displayPokemonStatProgressBar(false)
                    }
                }
            }
        })
    }

    private fun observePokemonTypes() {
        viewModel.observePokemonTypesDataState().observe(this, Observer {
            it?.let { pokemonTypesDataState ->
                when (pokemonTypesDataState) {
                    is Resource.Loading -> {
                        displayPokemonTypeProgressBar(true)
                    }

                    is Resource.Error -> {
                        showToast(pokemonTypesDataState.message)
                        displayPokemonTypeProgressBar(false)
                    }

                    is Resource.Success -> {
                        setViewWithPokemonTypes(pokemonTypesDataState.data)
                        displayPokemonTypeProgressBar(false)
                    }
                }
            }
        })
    }

    private fun setViewWithPokemon(pokemon: Pokemon) {
        tvDetailPokemonName.text = pokemon.name

        pokemon.image?.let { imagePath ->
            imgDetailPokemon.loadImageFromUrl(imagePath)
        }
    }

    private fun setViewWithPokemonTypes(pokemonTypes: List<PokemonType>) {
        rvPokemonTypes.adapter?.let { adapter ->
            when (adapter) {
                is PokemonTypeAdapter -> {
                    adapter.differ.submitList(pokemonTypes)
                }
            }
        }
    }

    private fun setViewWithPokemonStats(pokemonStats: List<PokemonStat>) {
        rvPokemonStats.adapter?.let { adapter ->
            when (adapter) {
                is PokemonStatAdapter -> {
                    adapter.differ.submitList(pokemonStats)
                }
            }
        }
    }

    private fun displayPokemonStatProgressBar(isDisplayed: Boolean) {
        if (isDisplayed) {
            rvPokemonStats.gone()
            statProgressBar.visible()
        } else {
            statProgressBar.gone()
            rvPokemonStats.visible()
        }
    }

    private fun displayPokemonTypeProgressBar(isDisplayed: Boolean) {
        if (isDisplayed) {
            rvPokemonTypes.gone()
            typeProgressBar.visible()
        } else {
            typeProgressBar.gone()
            rvPokemonTypes.visible()
        }
    }
}
