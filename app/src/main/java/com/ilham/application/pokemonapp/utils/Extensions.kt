package com.ilham.application.pokemonapp.utils

import android.content.Context
import android.graphics.Color
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import com.bumptech.glide.Glide

/**
 * View Extension
 */

fun ImageView.loadImageFromUrl(imagePath: String) {
    Glide.with(this).clear(this)
    Glide.with(this).load(imagePath).into(this)
}

fun Context.showToast(msg: String) {
    Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun ProgressBar.setStat(baseStat: Int) {
    this.progress = baseStat

    this.progressDrawable.setTint(
        when (baseStat) {
            0 -> Color.WHITE
            in 1..30 -> Color.CYAN
            in 31..60 -> Color.BLUE
            in 61..80 -> Color.GREEN
            in 81..100 -> Color.YELLOW
            else -> Color.RED
        }
    )
}
