package com.ilham.application.pokemonapp.ui.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.ilham.application.pokemonapp.R
import com.ilham.application.pokemonapp.model.PokemonType
import kotlinx.android.synthetic.main.item_row_type.view.*

class PokemonTypeAdapter :
    RecyclerView.Adapter<PokemonTypeAdapter.ListViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<PokemonType>() {
        override fun areItemsTheSame(oldItem: PokemonType, newItem: PokemonType): Boolean {
            return oldItem.slot == newItem.slot
        }

        override fun areContentsTheSame(oldItem: PokemonType, newItem: PokemonType): Boolean {
            return oldItem == newItem
        }

    }

    val differ = AsyncListDiffer(this, diffCallback)

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(data: PokemonType) {
            with(itemView) {
                tvPokemonType.text = data.name
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder =
        ListViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_row_type, parent, false)
        )

    override fun getItemCount(): Int = differ.currentList.size

    override fun onBindViewHolder(holder: PokemonTypeAdapter.ListViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

}