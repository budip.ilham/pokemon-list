package com.ilham.application.pokemonapp.ui.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.ilham.application.pokemonapp.data.PokemonRepository
import com.ilham.application.pokemonapp.model.Pokemon
import com.ilham.application.pokemonapp.vo.Resource

class MainViewModel @ViewModelInject constructor(
    private val pokemonRepository: PokemonRepository
) :
    ViewModel() {

    fun observeListDataState(): LiveData<Resource<List<Pokemon>>> =
        pokemonRepository.getListPokemon()

}