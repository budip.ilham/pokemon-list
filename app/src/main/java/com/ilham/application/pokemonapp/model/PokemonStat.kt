package com.ilham.application.pokemonapp.model

data class PokemonStat(
    val name: String? = null,
    val baseStat: Int? = null
)